#!/bin/bash

cd baby-sleep-monitor
app_name="cinchremit/baby-monitor"

week=$(date +"%V")
year=$(date +'%Y')
TAG=$(git describe --tags $(git rev-list --tags --max-count=1))

echo "$year-$week-v$TAG"

docker image build -t $app_name:$TAG -t $app_name:latest .
docker image push $app_name:$TAG
docker image push $app_name:latest