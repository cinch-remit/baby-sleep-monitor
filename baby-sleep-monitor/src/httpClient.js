export const doRequest = async (url, settings = null) => {
    return (fetch(url, settings));
}

export const defaultHeaders = {
    "Content-Type": "application/json",
}


export const API_URL = process.env.REACT_APP_API_URL;
export const AUTHORIZATION = 'Authorization';