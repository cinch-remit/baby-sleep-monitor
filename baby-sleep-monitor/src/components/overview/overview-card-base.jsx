import React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
const OverviewCardBase = ({ children }) => (
    <Box sx={{minWidth: 275}}>
        <Card variant="outlined">
            {children}
        </Card>
    </Box>
)

export default OverviewCardBase;