import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import moment from 'moment/moment';
import {DeviceThermostat, AccessTime, Timeline} from '@mui/icons-material';
import {LinearProgress, Stack} from "@mui/material";

const DeviceInfoCard = ({device}) => (
    <React.Fragment>
        {
            device && device?.readings?.length > 0 ?
                <CardContent>
                    <Typography variant="h6" sx={{mb: 1.5}} color="text.secondary">
                        <span><small style={{fontWeight: 'lighter'}}>Current temperature is</small></span> {device.readings[0].value}&#8451;
                        <DeviceThermostat color={"error"}/>
                    </Typography>
                    <Typography sx={{mb: 1.5}} color="text.secondary" variant={'h6'}>
                        <span><small style={{fontWeight: 'lighter'}}>Last reading was taken </small></span>
                        <time>{`${moment(device.readings[0].timeOfReading).format('YY-MM-DD')} `}</time>
                        <AccessTime color={"primary"}/>
                    </Typography>
                    <Typography sx={{mb: 1.5}} color="text.secondary" variant={"h6"}>
                        <span><small
                            style={{fontWeight: 'lighter'}}>This device has </small></span>{device.totalReadingsForDevice} readings <Timeline
                        color={"warning"}/>
                    </Typography>
                </CardContent>
                :
                <CardContent>
                    <Typography variant="h6" sx={{mb: 4}} color="text.secondary">
                        <span><small style={{fontWeight: 'lighter'}}>No readings yet for device</small></span>
                    </Typography>

                    <Stack spacing={2} alignContent={"center"}>
                        <Box sx={{width: '80%'}}>
                            <LinearProgress/>
                        </Box>

                        <span><small style={{fontWeight: 'lighter'}}>listening for new data..</small></span>

                    </Stack>
                </CardContent>

        }

    </React.Fragment>
);

const DeviceOverviewCard = ({device}) => {
    return (
        <Box sx={{minWidth: 275}}>
            <Card variant="outlined">
                <DeviceInfoCard device={device}/>
            </Card>
        </Box>
    );
}

export default DeviceOverviewCard;