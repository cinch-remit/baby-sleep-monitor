import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import {useDispatch, useSelector} from "react-redux";
import {doSelectDevice} from "../../actions/device/deviceActions";

const DeviceTab = () => {
    const [value, setValue] = React.useState(0);
    const { devices } = useSelector(state => state.devices);

    const dispatch = useDispatch();

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ maxWidth: 480, bgcolor: 'background.paper' }}>
            {
                devices.length > 0 ?
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                    >
                        {
                            devices.map((device, index) => <Tab key={index} label={`${device.deviceID}`} onClick={() => dispatch(doSelectDevice(device.deviceID))}/>)
                        }
                    </Tabs>
                    :
                    null
            }

        </Box>
    );
}

export default DeviceTab;