import React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import {useDispatch, useSelector} from "react-redux";
import {doSelectDashboard} from "../../actions/dashboard/dashboardActions";

const DashboardTab = () => {
    const [value, setValue] = React.useState(0);
    const {dashboards} = useSelector(state => state.dashboard);

    const dispatch = useDispatch();

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{maxWidth: 480, bgcolor: 'background.paper'}}>
            {
                dashboards.length > 0 ?
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                    >
                        {
                            dashboards.map((dashboard, index) => <Tab key={index} label={`${dashboard.name}`}
                                                                      onClick={() => {
                                                                          dispatch(doSelectDashboard(dashboard.name));
                                                                      }}/>)
                        }
                    </Tabs>
                    :
                    null
            }

        </Box>
    );
}

export default DashboardTab;