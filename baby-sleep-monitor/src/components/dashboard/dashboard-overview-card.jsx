import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {Timeline, Router, BarChart} from '@mui/icons-material';
import {LinearProgress, Stack} from "@mui/material";

const DashboardInfoCard = ({dashboard}) => (
    <React.Fragment>
        {
            dashboard.deviceId  ?
                <CardContent>
                    <Typography variant="h6" sx={{mb: 1.5}} color="text.secondary">
                        <span><small style={{fontWeight: 'lighter'}}>Device: </small></span> {dashboard.deviceId}
                        <Router color={"info"}/>
                    </Typography>
                    <Typography sx={{mb: 1.5}} color="text.secondary" variant={'h6'}>
                        <span><small style={{fontWeight: 'lighter'}}>Type of reading: </small></span>
                        {dashboard.typeOfReading}
                        <BarChart color={"info"}/>
                    </Typography>
                    <Typography sx={{mb: 1.5}} color="text.secondary" variant={"h6"}>
                        <span><small
                            style={{fontWeight: 'lighter'}}>Metric: </small></span>{dashboard.metric} <Timeline color={"info"}/>
                    </Typography>
                </CardContent>
                :
                <CardContent>
                    <Typography variant="h6" sx={{mb: 4}} color="text.secondary">
                        <span><small style={{fontWeight: 'lighter'}}>No saved dashboards</small></span>
                    </Typography>

                    <Stack spacing={2} alignContent={"center"}>
                        <Box sx={{width: '80%'}}>
                            <LinearProgress/>
                        </Box>

                        <span><small style={{fontWeight: 'lighter'}}>listening for new data..</small></span>

                    </Stack>
                </CardContent>

        }

    </React.Fragment>
);

const DashboardOverviewCard = ({dashboard}) => {
    return (
        <Box sx={{minWidth: 275}}>
            <Card variant="outlined">
                <DashboardInfoCard dashboard={dashboard}/>
            </Card>
        </Box>
    );
}

export default DashboardOverviewCard;