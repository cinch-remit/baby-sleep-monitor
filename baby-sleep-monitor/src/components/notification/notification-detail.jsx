import React from "react";

import {List, ListItem, ListItemText} from "@mui/material";

const NotificationDetail = ({ notification }) => {
    return (<React.Fragment>
        <List>
            {
                Object.keys(notification.details).map((key) => {
                        return <React.Fragment key={key}>
                            {
                                <ListItem button key={notification.details[key]}>
                                    <ListItemText primary={key} secondary={notification.details[key]}/>
                                </ListItem>

                            }
                        </React.Fragment>
                    }
                )
            }
        </List>
    </React.Fragment>)
}


export default NotificationDetail;