import React from "react";

import {List, ListItem, ListItemText} from "@mui/material";
import {useSelector} from "react-redux";

const ProfileInfo = () => {
    const { user } = useSelector(state => state.user);
    const {email, roles, name } = user;

    return (<React.Fragment>
        <List>
            <ListItem button key={email}>
                <ListItemText primary={'Email'} secondary={email}/>
            </ListItem>

            <ListItem>
                <ListItemText primary={'Access'} secondary={roles.toString()}/>
            </ListItem>

            {
                name ?
                    <ListItem button key={name}>
                        <ListItemText primary={'Name'} secondary={name}/>
                    </ListItem>
                    :
                    null
            }

        </List>
    </React.Fragment>)
}


export default ProfileInfo;