import React from "react";
import {Stack} from "@mui/material";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {useSelector} from "react-redux";

const AppInfoOverview = () => {
    const { buildInfo } = useSelector(state => state.appInfo);
    const { buildId, version } = buildInfo;

    return (
        <React.Fragment>
            {
                buildId ?
                    <Box>
                        <Stack style={{
                            textAlign: 'center'
                        }}>
                            <Typography variant="overline" display="block" gutterBottom sx={{fontSize: 9}}>
                                {`version: ${version}`}
                            </Typography>

                            <Typography variant="overline" display="block" gutterBottom sx={{fontSize: 9}}>
                                {`build: ${buildId}`}
                            </Typography>
                        </Stack>
                    </Box>
                    :
                    null
            }
        </React.Fragment>
    )
}

export default AppInfoOverview;