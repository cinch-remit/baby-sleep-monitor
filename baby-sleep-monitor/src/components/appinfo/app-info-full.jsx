import React from "react";

import {List, ListItem, ListItemText} from "@mui/material";
import {useSelector} from "react-redux";

const AppInfoFull = () => {
    const { buildInfo} = useSelector(state => state.appInfo);
    const { buildId, version, build, developedBy, contact, leadDeveloper, location } = buildInfo;

    return (<React.Fragment>
        <List>
            <ListItem button key={version}>
                <ListItemText primary={'Version'} secondary={version}/>
            </ListItem>

            <ListItem button key={build}>
                <ListItemText primary={'Build'} secondary={build}/>
            </ListItem>

            <ListItem button key={buildId}>
                <ListItemText primary={'Build ID'} secondary={buildId}/>
            </ListItem>

            <ListItem button key={developedBy}>
                <ListItemText primary={'Company'} secondary={developedBy}/>
            </ListItem>

            <ListItem button key={leadDeveloper}>
                <ListItemText primary={'Lead Developer'} secondary={`${leadDeveloper} ${contact}`}/>
            </ListItem>

            <ListItem button key={location}>
                <ListItemText primary={'Location'} secondary={location}/>
            </ListItem>

        </List>
    </React.Fragment>)
}


export default AppInfoFull;