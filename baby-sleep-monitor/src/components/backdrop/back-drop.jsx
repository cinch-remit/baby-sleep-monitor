import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from "@mui/material/Typography";
import {Stack} from "@mui/material";

const BackDrop = ({ open, message }) => {
    return (
        <div>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
            >
                <Stack spacing={3}>
                    <CircularProgress color="inherit" />
                    <Typography sx={{textAlign: "center"}}>
                        {message}
                    </Typography>
                </Stack>


            </Backdrop>
        </div>
    );
}

export default BackDrop;