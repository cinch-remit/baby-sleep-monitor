import React, {useEffect} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';

import Toolbar from '@mui/material/Toolbar';

import IconButton from '@mui/material/IconButton';

import MenuIcon from '@mui/icons-material/Menu';

import NotificationIcon from '@mui/icons-material/Notifications';
import Logout from '@mui/icons-material/Logout';
import Badge from '@mui/material/Badge';

import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import NavDrawerList from "./nav-drawer-list";
import {useDispatch, useSelector} from "react-redux";
import {doLoadNotifications, doLogout} from "../../actions/user/userActions";
import {useHistory} from "react-router-dom";

const BottomAppBar = ({ handleAppInfoDialogState, handleProfileDialogState }) => {
    const [pos, setPos] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const dispatch = useDispatch();
    const { notifications, loggedIn } = useSelector(state => state.user);
    useEffect(() => {
            if (notifications.length === 0 && loggedIn) {
                dispatch(doLoadNotifications())
            }
        },
        // eslint-disable-next-line
        [loggedIn]);

    const toggleDrawer = (anchor, open) => (event) => {
        if (
            event &&
            event.type === 'keydown' &&
            (event.key === 'Tab' || event.key === 'Shift')
        ) {
            return;
        }

        setPos({ ...pos, [anchor]: open });
    };

    const history = useHistory();

    const position = 'left';
    const unreadNotifications = notifications.filter(notification => !notification.read);

    return (
        <React.Fragment>
            <AppBar position="fixed" color="primary" sx={{ top: 'auto', bottom: 0 }}>
                <Toolbar>
                    <IconButton color="inherit" aria-label="open drawer" onClick={toggleDrawer(position, true)}>
                        <MenuIcon />
                    </IconButton>

                    {
                        notifications.length > 0 ?
                            <IconButton color="inherit" onClick={() => history.push('/notifications')}>
                                <Badge badgeContent={unreadNotifications.length} color="error">
                                    <NotificationIcon />
                                </Badge>
                            </IconButton>
                            :
                            null
                    }

                    <Box sx={{ flexGrow: 1 }} />

                    <IconButton color="inherit" onClick={() => dispatch(doLogout())}>
                        <Logout />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <SwipeableDrawer
                anchor={position}
                open={pos[position]}
                onClose={toggleDrawer(position, false)}
                onOpen={toggleDrawer(position, true)}
            >
                <NavDrawerList anchor={position}
                               toggleDrawer={toggleDrawer}
                               handleAppInfoDialogState={handleAppInfoDialogState}
                               handleProfileDialogState={handleProfileDialogState}/>
            </SwipeableDrawer>
        </React.Fragment>
    );
}

export default BottomAppBar;