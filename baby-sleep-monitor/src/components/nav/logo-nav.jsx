import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import logo from '../../assets/images/logo/logo.png'
import {Avatar} from "@mui/material";
import {useHistory} from "react-router-dom";


const LogoNav = () => {
    const history = useHistory();

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" color={"transparent"} onClick={() => history.push("/")}>
                <Toolbar>
                    <Typography variant="h6"
                                component="div"
                                sx={{ flexGrow: 1, cursor: 'pointer' }}>
                        Baby Sleep Monitor
                    </Typography>
                    <Avatar src={logo} sx={{ cursor: 'pointer'}}/>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export default LogoNav;