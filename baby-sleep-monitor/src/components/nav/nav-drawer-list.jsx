import React,{useState} from 'react';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import ProfileIcon from '@mui/icons-material/Person';
import AppInfoIcon from '@mui/icons-material/Info';
import Home from '@mui/icons-material/Home';
import {Notifications, Dashboard, Router, Logout} from '@mui/icons-material';

import AppInfoOverview from "../appinfo/app-info-overview";
import {useDispatch} from "react-redux";
import {doLogout} from "../../actions/user/userActions";
import {Redirect} from "react-router";

const NavDrawerList = ({toggleDrawer, anchor, handleAppInfoDialogState, handleProfileDialogState}) => {
    const dispatch = useDispatch();

    const [redirect, setRedirect] = useState(false);
    const [link, setLink] = useState('');

    const options = [{name: 'Devices'}, {name: 'Dashboards'}, {name: 'Notifications'}];
    const optionsListItems = options.map(({name}, key) =>
        <ListItem button key={key} onClick={() => {
            if (name === "Home" || name === 'Devices') {
                setLink('/');
                setRedirect(true);
            }

            if (name === "Dashboards") {
                setLink('/dashboards');
                setRedirect(true);
            }

            if (name === "Notifications") {
                setLink('/notifications');
                setRedirect(true);
            }
        }}>
            <ListItemIcon>
                {
                        name === "Devices" ?
                            <Router color={"secondary"}/>
                            :
                            name === "Dashboards" ?
                                <Dashboard color={"secondary"}/>
                                :
                                name === 'Home' ?
                                    <Home color={"info"}/>
                                    :
                                <Notifications color={"secondary"}/>
                }
            </ListItemIcon>
            <ListItemText primary={name}/>
        </ListItem>
    );

    const personal = [{name: 'Profile'}, {name: 'App Information'},{name: 'Logout'}];
    const personalListItems = personal.map(({name}, key) =>
        <ListItem button key={key} onClick={() => {
            if (name === "Logout") {
                dispatch(doLogout());
            }

            if (name === "App Information") {
                handleAppInfoDialogState(true)
            }

            if (name === "Profile") {
                handleProfileDialogState(true);
            }
        }}>
            <ListItemIcon>
                {
                    name === 'Profile' ?
                        <ProfileIcon color={"primary"}/>
                        :
                            name === "Logout" ?
                                <Logout color={"error"}/>
                                :
                                <AppInfoIcon color={"primary"}/>
                }
            </ListItemIcon>
            <ListItemText primary={name}/>
        </ListItem>
    )

    if (redirect) {
        return <Redirect to={link}/>
    }

    return (
        <Box
            sx={{width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250}}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                {optionsListItems}
            </List>
            <Divider/>
            <List>
                {personalListItems}
            </List>

            <Divider/>
            <div style={{
                marginBottom: '3rem'
            }}/>
            <AppInfoOverview/>
        </Box>);
}

export default NavDrawerList;
