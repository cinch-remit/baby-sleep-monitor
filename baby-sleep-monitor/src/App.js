import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Homepage from "./pages/Homepage";
import LoginPage from "./pages/LoginPage";
import BackDrop from "./components/backdrop/back-drop";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {doSetBuildInfo} from "./actions/info/appinfoActions";
import {Redirect} from "react-router";
import {AUTHORIZATION} from "./httpClient";
import {doAutoLogin} from "./actions/user/userActions";
import LogoNav from "./components/nav/logo-nav";
import BottomAppBar from "./components/nav/bottom-app-bar";
import DashboardPage from "./pages/DashboardPage";
import NotificationsPage from "./pages/NotificationsPage";
import {Dialog, DialogContent, DialogTitle, Divider} from "@mui/material";
import AppInfoFull from "./components/appinfo/app-info-full";
import ProfileInfo from "./components/profile/profile-info";

function App() {
    const { loading } = useSelector(state => state.appInfo);
    const { loggedIn } = useSelector(state => state.user);

    const auth = localStorage.getItem(AUTHORIZATION);

    const [openAppInfoDialog, setOpenAppInfoDialog] = useState(false);
    const [openProfileDialog, setOpenProfileDialog] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(doSetBuildInfo())
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        if (auth && !loggedIn) {
            dispatch(doAutoLogin());
        }
        // eslint-disable-next-line
    }, [loggedIn, auth]);

  return (
      <Router>
          <LogoNav/>
          <div>
          <Switch>
              <Route path={'/'} exact>
                  {!loggedIn ? <Redirect to={'/login'}/> : <Homepage/>}
              </Route>

              <Route path={'/dashboards'} exact>
                  {!loggedIn ? <Redirect to={'/login'}/> : <DashboardPage/>}
              </Route>

              <Route path={'/notifications'} exact>
                  {!loggedIn ? <Redirect to={'/login'}/> : <NotificationsPage/>}
              </Route>

              <Route path={'/login'} exact component={LoginPage}/>
          </Switch>
              {
                  loggedIn ?
                      <BottomAppBar handleAppInfoDialogState={setOpenAppInfoDialog} handleProfileDialogState={setOpenProfileDialog}/>
                      :
                      null
              }

          </div>

          <BackDrop open={loading} message={'....'}/>

          <Dialog open={openAppInfoDialog} onClose={() => {
              setOpenAppInfoDialog(false);
          }}>
              <DialogTitle>
                  App Information
              </DialogTitle>
              <Divider/>
              <DialogContent>
                  <AppInfoFull/>
              </DialogContent>
          </Dialog>

          <Dialog open={openProfileDialog} onClose={() => {
              setOpenProfileDialog(false);
          }}>
              <DialogTitle>
                  Profile Information
              </DialogTitle>
              <Divider/>
              <DialogContent>
                  <ProfileInfo/>
              </DialogContent>
          </Dialog>
      </Router>
  );
}

export default App;
