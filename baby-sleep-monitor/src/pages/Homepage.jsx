import React, {useEffect, useState} from 'react';

import CssBaseline from '@mui/material/CssBaseline';

import Paper from '@mui/material/Paper';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';

import {DeviceThermostat, ChildFriendly, ChildCare} from '@mui/icons-material'

import moment from "moment/moment";
import {ListItemIcon, Stack, Switch} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import DeviceTab from "../components/device/device-tab";
import DeviceOverviewCard from "../components/device/device-overview-card";
import {loadDevices} from "../actions/device/deviceActions";
import Box from "@mui/material/Box";

const Homepage = () => {
    const [readings, setReadings] = useState([]);
    const [device, setDevice] = useState({});

    const [showConnected, setShowConnected] = useState(false);

    const {devices, selectedDeviceId} = useSelector(state => state.devices);

    const handleSetShowConnected = event => {
        setShowConnected(event.target.checked);
    }

    const dispatch = useDispatch();

    useEffect(() => {
        if (devices.length === 0) {
            dispatch(loadDevices());
        }
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        if (selectedDeviceId !== '') {
            const selected = devices.find(d => d.deviceID === selectedDeviceId);
            setDevice(selected);
            setReadings(selected.readings);
        }
    }, [devices, selectedDeviceId])

    const sortedByTime = () => {
        let data = {};

        readings.forEach(({value, timeOfReading, typeOfReading, readingId, metric}, index) => {
            const key = moment(timeOfReading).format('YYYY-MM-DD');
            const keys = Object.keys(data);
            const time = moment(timeOfReading).format("hh:mm:ss");

            if (keys.indexOf(key) > -1) {
                const previous = data[key];

                data[key] = [...previous, {
                    value, typeOfReading, metric, time
                    , readingId
                }];
            } else {
                data[key] = [{value, typeOfReading, metric, time, readingId}];
            }
        });
        return data;
    }

    const sortedByConnection = () => {
        let data = {};
        const sortedReadings = [...readings].filter(reading => reading.connected);
        sortedReadings.forEach(({
                                       value,
                                       timeOfReading,
                                       typeOfReading,
                                       readingId,
                                       metric,
                                       connected,
                                       connectionId
                                   }, index) => {
            const key = connectionId;
            const keys = Object.keys(data);
            const time = moment(timeOfReading).format("hh:mm:ss");

            if (keys.indexOf(key) > -1) {
                const previous = data[key];

                data[key] = [...previous, {value, typeOfReading, metric, time, readingId, connected, connectionId}];
            } else {
                data[key] = [{value, typeOfReading, metric, time, readingId, connected, connectionId}];
            }
        });
        return data;
    }

    return (
        <React.Fragment>
            <CssBaseline/>
            <Paper square sx={{pb: '50px'}}>
                <div style={{
                    margin: '5px', padding: '5px'
                }}>
                    <DeviceTab/>
                    <DeviceOverviewCard device={device}/>
                </div>

                {
                    Object.keys(sortedByConnection()).length > 0 ?
                        <Box sx={{maxWidth: 480, bgcolor: 'background.paper', pl: 2}}>
                            <Stack spacing={2} direction={"row"}>
                                <div>
                            <span style={{
                                fontWeight: 'lighter',
                                fontSize: '12',
                            }}>all</span>
                                    <Switch color={"primary"} checked={showConnected}
                                            onChange={handleSetShowConnected}/>
                                       <span style={{
                                        fontWeight: 'lighter',
                                        fontSize: '12'
                                    }}>connected</span>
                                </div>
                            </Stack>
                        </Box>
                        :
                        null
                }

                {
                    showConnected ?
                        <List sx={{mb: 2}}>
                            {
                                Object.keys(sortedByConnection()).map((connectionId, index) => {
                                    let data = sortedByConnection();
                                    return <React.Fragment key={connectionId}>
                                        <ListSubheader sx={{bgcolor: 'background.paper'}}>
                                            {connectionId.toUpperCase()}
                                        </ListSubheader>
                                        {
                                            data[connectionId].map(reading => <ListItem button key={reading.readingId}>
                                                <ListItemIcon>
                                                    {
                                                        reading.metric === 'Motion' ?
                                                            <React.Fragment>
                                                                {
                                                                    reading.value === 'True' ?
                                                                        <ChildCare color={"info"}/>
                                                                        :
                                                                        <ChildFriendly color={"success"}/>
                                                                }
                                                            </React.Fragment>
                                                            :
                                                            <DeviceThermostat color={"error"}/>
                                                    }

                                                </ListItemIcon>
                                                {
                                                    reading.metric === 'Motion' ?
                                                        <React.Fragment>
                                                            {
                                                                reading.value === 'True' ?
                                                                    <ListItemText primary={`Baby awake`}
                                                                                  secondary={reading.time}/>
                                                                    :
                                                                    <ListItemText primary={`Baby asleep`}
                                                                                  secondary={reading.time}/>
                                                            }
                                                        </React.Fragment>
                                                        :
                                                        <ListItemText primary={`${reading.value} ${reading.metric}`}
                                                                      secondary={reading.time}/>
                                                }
                                            </ListItem>)
                                        }
                                    </React.Fragment>
                                })
                            }
                        </List>
                        :
                        <List sx={{mb: 2}}>
                            {
                                Object.keys(sortedByTime()).map((time, index) => {
                                    let data = sortedByTime();
                                    return <React.Fragment key={time}>
                                        <ListSubheader sx={{bgcolor: 'background.paper'}}>
                                            {time}
                                        </ListSubheader>
                                        {
                                            data[time].map(reading => <ListItem button key={reading.readingId}>
                                                <ListItemIcon>
                                                    {
                                                        reading.metric === 'Motion' ?
                                                            <React.Fragment>
                                                                {
                                                                    reading.value === 'True' ?
                                                                        <ChildCare color={"info"}/>
                                                                        :
                                                                        <ChildFriendly color={"success"}/>
                                                                }
                                                            </React.Fragment>
                                                            :
                                                            <DeviceThermostat color={"error"}/>
                                                    }

                                                </ListItemIcon>
                                                {
                                                    reading.metric === 'Motion' ?
                                                        <React.Fragment>
                                                            {
                                                                reading.value === 'True' ?
                                                                    <ListItemText primary={`Baby awake`}
                                                                                  secondary={reading.time}/>
                                                                    :
                                                                    <ListItemText primary={`Baby asleep`}
                                                                                  secondary={reading.time}/>
                                                            }
                                                        </React.Fragment>
                                                        :
                                                        <ListItemText primary={`${reading.value} ${reading.metric}`}
                                                                      secondary={reading.time}/>
                                                }
                                            </ListItem>)
                                        }
                                    </React.Fragment>
                                })
                            }
                        </List>
                }

            </Paper>
        </React.Fragment>
    );
}

export default Homepage;