import React, {useEffect, useState} from "react";

import Box from "@mui/material/Box";
import {Button, Container, CssBaseline, Stack, TextField} from "@mui/material";
import { Fingerprint } from '@mui/icons-material';
import AppInfoOverview from "../components/appinfo/app-info-overview";
import {API_URL, defaultHeaders, doRequest} from "../httpClient";
import {validateEmail} from "../validations";
import {useDispatch, useSelector} from "react-redux";
import {doSetGlobalLoading} from "../actions/info/appinfoActions";
import {doSetUser} from "../actions/user/userActions";
import {Redirect} from "react-router";

const LoginPage = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loginError, setLoginError] = useState(false);
    const [redirectToHomepage, setRedirectToHomepage] = useState(false);

    const { loading } = useSelector(state => state.appInfo);
    const { loggedIn } = useSelector(state => state.user);

    const dispatch = useDispatch();

    const adminLink = 'https://iot.ebrinktech.com/forgot';

    const login = () => {
        if (!validateEmail(email) || password === '') {
            return;
        }

        dispatch(doSetGlobalLoading(true));

        const data = new Buffer(`${email}:${password}`);
        const settings = {
            method: "GET",
            headers: {
                ...defaultHeaders,
                Authorization: `Basic ${data.toString('base64')}`
            }
        }

        doRequest(`${API_URL}v1/user/details`, settings)
            .then(res => {
                if (res.ok) {
                    return res.json();
                }
                throw res;
            })
            .then(details => {
                localStorage.setItem('Authorization', `Basic ${data.toString('base64')}`);
                dispatch(doSetUser(details));
                dispatch(doSetGlobalLoading(false));
                setRedirectToHomepage(true);
            })
            .catch(() => {
                setLoginError(true);
                dispatch(doSetGlobalLoading(false));
            })
    }

    useEffect(() => {
        if (loggedIn) {
            setRedirectToHomepage(true);
        }
    }, [loggedIn]);

    if (redirectToHomepage) {
        return <Redirect to={'/'}/>
    }

    return (
        <React.Fragment>
            <CssBaseline/>
            <Container maxWidth="sm">
                <Box sx={{height: '50vh', mt: 8}}>
                    <div>
                            <Stack spacing={3}>
                                <TextField
                                    required
                                    id="outlined-required"
                                    label="Email"
                                    placeholder="username@email.com"
                                    autoComplete={"current-email"}
                                    color={"info"}
                                    type={"email"}
                                    value={email}
                                    onChange={e => {
                                        if (loginError){
                                            setLoginError(false);
                                        }
                                        setEmail(e.target.value);
                                    }}
                                    disabled={loading}
                                    error={email !== '' && !validateEmail(email)}
                                />

                                <TextField
                                    required
                                    id="outlined-password-input"
                                    label="Password"
                                    type="password"
                                    autoComplete="current-password"
                                    placeholder={'*********'}
                                    color={"info"}
                                    value={password}
                                    onChange={e => {
                                        if (loginError){
                                            setLoginError(false);
                                        }
                                        setPassword(e.target.value)
                                    }}
                                    disabled={loading}
                                    error={email !== '' && password === ''}
                                />

                                <div style={{textAlign: 'center'}}>
                                    <Button color={"info"} startIcon={<Fingerprint />}
                                            size={"large"}
                                            variant={"contained"}
                                            disabled={loading  || !validateEmail(email) || password === ''}
                                            onClick={login}>
                                        login
                                    </Button>
                                </div>

                                {
                                    loginError ?
                                        <Button variant={"text"}
                                                color={"error"}
                                                sx={{textTransform: "lowercase"}}
                                                size={"medium"}>
                                            invalid email/password
                                        </Button>
                                        :
                                        null
                                }

                                <Button variant={"text"}
                                        href={adminLink}
                                        size={"small"}
                                        sx={{textTransform: "lowercase"}}
                                        target={"_blank"}
                                        disabled={loading}>
                                    forgot your password?
                                </Button>
                            </Stack>
                    </div>
                </Box>
            </Container>
            <Container>
                <div style={{
                    marginBottom: '3rem'
                }}/>
                <AppInfoOverview/>
            </Container>
        </React.Fragment>
    )
}

export default LoginPage;