import React, {useEffect, useState} from 'react';

import CssBaseline from '@mui/material/CssBaseline';

import Paper from '@mui/material/Paper';
import {useDispatch, useSelector} from "react-redux";

import {loadDashboards} from "../actions/dashboard/dashboardActions";
import DashboardTab from "../components/dashboard/dashboard-tab";
import DashboardOverviewCard from "../components/dashboard/dashboard-overview-card";


const DashboardPage = () => {
    const [selectedDashboard, setSelectedDashboard] = useState({});

    const { dashboards, selectedDashboardName } = useSelector(state => state.dashboard);

    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(loadDashboards());
        // eslint-disable-next-line
    },[]);

    useEffect(() => {
        if (selectedDashboardName !== '') {
            const selected = dashboards.find(d => d.name === selectedDashboardName);
            setSelectedDashboard(selected);
        }
    }, [dashboards, selectedDashboardName])


    return (
        <React.Fragment>
            <CssBaseline />
            <Paper square sx={{ pb: '50px' }}>
                <div style={{
                    margin: '5px',padding: '5px'
                }}>
                    <DashboardTab/>
                    <DashboardOverviewCard dashboard={selectedDashboard}/>
                </div>
            </Paper>
        </React.Fragment>
    );
}

export default DashboardPage;