import React from "react";
import {useSelector} from "react-redux";
import {Redirect} from "react-router";

const RedirectToLoginPage = ({...children}) => {
    const { loggedIn } = useSelector(state => state.loggedIn);
    if (!loggedIn) {
        return <Redirect to={'/login'}/>
    }
    return {children}
};

export default RedirectToLoginPage;