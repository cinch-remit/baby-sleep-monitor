import React, {useState} from 'react';

import CssBaseline from '@mui/material/CssBaseline';

import Paper from '@mui/material/Paper';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';

import {MarkChatUnread, MarkChatRead} from '@mui/icons-material'

import moment from "moment/moment";
import {Dialog, DialogContent, DialogTitle, Divider, ListItemIcon, Stack, Switch, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import Box from "@mui/material/Box";
import NotificationDetail from "../components/notification/notification-detail";

import {doReadNotification} from "../actions/user/userActions";


const NotificationsPage = () => {

    const {notifications} = useSelector(state => state.user);
    const [unreadChecked, setUnreadChecked] = useState(false);

    const dispatch = useDispatch();

    const [selectedNotification, setSelectedNotification] = useState({});
    const [openDialog, setOpenDialog] = useState(false);

    const readNotification = notificationId => {
        const selected = notifications.find(n => n.id === notificationId);
        setSelectedNotification(selected);
        setOpenDialog(true);
    }

    const sortedByTime = (unread = false) => {
        let source = notifications;
        if (unread) {
            source = [...notifications].filter(notification => !notification.read);

            if (source.length === 0) {
                setUnreadChecked(false);
                source = notifications;
            }
        }
        let data = {};

        source.forEach(({details, timestamp, read, type, iotId, id}) => {
            const key = moment(timestamp).format('YYYY-MM-DD');
            const keys = Object.keys(data);
            const time = moment(timestamp).format("hh:mm:ss");

            if (keys.indexOf(key) > -1) {
                const previous = data[key];

                data[key] = [...previous, {details, type, read, time, iotId, id}];
            } else {
                data[key] = [{details, type, read, time, iotId, id}];
            }
        });
        return data;
    }

    const unreadNotifications = notifications.filter(notification => !notification.read);
    const handleUnreadChecked = event => {
        setUnreadChecked(event.target.checked);
    }

    return (
        <React.Fragment>
            <CssBaseline/>
            <Paper square sx={{pb: '50px'}}>
                <div style={{
                    margin: '5px', padding: '5px'
                }}/>

                <Box sx={{maxWidth: 480, bgcolor: 'background.paper', pl: 2}}>
                    {
                        notifications.length > 0 ?
                            <React.Fragment>
                                <Typography color="text.secondary" variant={'h6'}>
                                    {`${unreadNotifications.length} Unread ${notifications.length === 1 ? 'Notification': 'Notifications'}`}
                                </Typography>

                                <Stack spacing={2} direction={"row"}>

                                    <div>
                            <span style={{
                                fontWeight: 'lighter',
                                fontSize: '12'
                            }}>all</span>
                                        <Switch color={"primary"} checked={unreadChecked}
                                                onChange={handleUnreadChecked}/>
                                        <span style={{
                                        fontWeight: 'lighter',
                                        fontSize: '12'
                                    }}>unread</span>
                                    </div>

                                </Stack>
                            </React.Fragment>
                            :
                            <React.Fragment>
                                <Typography color="text.secondary" variant={'h6'}>
                                    You have no notifications
                                </Typography>
                                <Typography variant="h6" color="text.secondary">
                                    <span><small style={{fontWeight: 'lighter'}}>We'll let you know when new notifications are available</small></span>
                                </Typography>
                            </React.Fragment>
                    }
                </Box>

                {
                    notifications.length > 0 ?
                        <List sx={{mb: 2}}>
                            {
                                Object.keys(sortedByTime()).map((time, index) => {
                                    let data = sortedByTime(unreadChecked);
                                    return <React.Fragment key={time}>
                                        <ListSubheader sx={{bgcolor: 'background.paper'}}>
                                            {time}
                                        </ListSubheader>
                                        {
                                            data[time].map((notification,i) => <ListItem button key={i} onClick={() => readNotification(notification.id)}>
                                                <ListItemIcon>
                                                    {
                                                        notification.read ?
                                                            <MarkChatRead color={'transparent'}/>
                                                            :
                                                            <MarkChatUnread color={'primary'}/>
                                                    }

                                                </ListItemIcon>
                                                <ListItemText primary={`${notification.type}`}
                                                              secondary={notification.time}/>
                                            </ListItem>)
                                        }
                                    </React.Fragment>
                                })
                            }
                        </List>
                        :
                        null
                }

            </Paper>

            <Dialog open={openDialog} onClose={() => {
                if (!selectedNotification.read) {
                    dispatch(doReadNotification(selectedNotification.id))
                }
                setOpenDialog(false);
            }}>
                <DialogTitle>
                    {selectedNotification.type}
                </DialogTitle>
                <Divider/>
                <DialogContent>
                    <NotificationDetail notification={selectedNotification}/>
                </DialogContent>
            </Dialog>
        </React.Fragment>
    );
}

export default NotificationsPage;