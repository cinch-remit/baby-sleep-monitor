import {API_URL, defaultHeaders, doRequest} from "../../httpClient";
import {doSetGlobalLoading} from "../info/appinfoActions";
import {doClearDevice} from "../device/deviceActions";
import {doClearDashboards} from "../dashboard/dashboardActions";

export const LOGIN = 'LOGIN';
export const CLEAR_USER = 'CLEAR_USER';
export const STATUS = 'STATUS';
export const FAILED = 'FAILED';

export const setUser = payload => ({
    type: LOGIN,
    payload
});

const logout = () => ({
    type: CLEAR_USER
});

export const doLogout = () => dispatch => {
    localStorage.clear();
    dispatch(logout());
    dispatch(doClearDevice());
    dispatch(doClearDashboards());
}

export const doSetUser = payload => ({
    type: LOGIN,
    payload
});

export const doAutoLogin = () => dispatch => {
    const authorization = localStorage.getItem('Authorization');
    if (!authorization) {
        return;
    }

    dispatch(doSetGlobalLoading(true));

    const settings = {
        method: "GET",
        headers: {
            ...defaultHeaders,
            Authorization: authorization
        }
    }

    doRequest(`${API_URL}v1/user/details`, settings)
        .then(res => {
            if (res.ok) {
                return res.json()
            }
            throw res;
        })
        .then(data => {
            dispatch(setUser(data));
        })
        .then(() => dispatch(doSetGlobalLoading(false)))
        .catch(err => {
            console.log(err);
            dispatch(doSetGlobalLoading(false));
        })
}

// user notification
export const UPDATE_NOTIFICATION = "UPDATE_NOTIFICATION";
export const LOAD_NOTIFICATIONS = "LOAD_NOTIFICATIONS";
const updateNotification = payload => ({
    type: UPDATE_NOTIFICATION,
    payload
});

const loadNotifications = payload => ({
    type: LOAD_NOTIFICATIONS,
    payload
})

export const doUpdateNotification = payload => dispatch => {
    dispatch(updateNotification(payload));
}

export const doLoadNotifications = () => dispatch => {
    const authorization = localStorage.getItem('Authorization');
    if (!authorization) {
        return;
    }

    const settings = {
        method: "GET",
        headers: {
            ...defaultHeaders,
            Authorization: authorization
        }
    }

    doRequest(`${API_URL}v1/notifications`, settings)
        .then((res) => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then((res) => {
            dispatch(loadNotifications(res))
        })
        .catch(() => console.error("could not load notifications."));
}


export const doReadNotification = notificationId => dispatch => {
    const authorization = localStorage.getItem('Authorization');
    if (!authorization) {
        return;
    }

    const settings = {
        method: "PUT",
        headers: {
            ...defaultHeaders,
            Authorization: authorization
        }
    }

    doRequest(`${API_URL}v1/notifications/read/${notificationId}`, settings)
        .then((res) => {
            if (res.ok) {
                dispatch(doLoadNotifications())
            }
            throw res;
        })
        .catch(() => console.error("could not load notifications."));
}