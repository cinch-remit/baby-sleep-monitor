import {API_URL, defaultHeaders, doRequest} from "../../httpClient";

export const SET_DASHBOARDS = 'SET_DASHBOARDS';
export const UPDATE_DASHBOARD = 'UPDATE_DASHBOARD';
export const CLEAR_DASHBOARDS = 'CLEAR_DASHBOARDS';
export const SELECT_DASHBOARD = 'SELECT_DASHBOARD';


const setDashboards = payload => ({
    type: SET_DASHBOARDS,
    payload
});

const updateDashboard = payload => ({
    type: UPDATE_DASHBOARD,
    payload
});

const selectDashboard = payload => ({
    type: SELECT_DASHBOARD,
    payload
});

export const doSelectDashboard = payload => dispatch => {
    dispatch(selectDashboard(payload));
}

 const clearDashboards = () => ({
    type: CLEAR_DASHBOARDS
});

export const doClearDashboards = () => dispatch => {
    dispatch(clearDashboards());
}

export const doSetDashboards = dashboards => dispatch => {
    dispatch(setDashboards(dashboards));
}

export const loadDashboards = () => dispatch => {
    const authorization = localStorage.getItem('Authorization');
    if (!authorization) {
        return;
    }
    const settings = {
        method: 'GET',
        headers: {
            ...defaultHeaders,
            Authorization: authorization
        }
    }

    doRequest(`${API_URL}v1/dashboard`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => {
            if (data.length > 0) {
                dispatch(setDashboards(data));
            }
        })
        .catch(err => console.log(err));
}

export const loadDashboard = dashboardName => dispatch => {
    const authorization = localStorage.getItem('Authorization');
    if (!authorization) {
        return;
    }
    const settings = {
        method: 'GET',
        headers: {
            ...defaultHeaders,
            Authorization: authorization
        }
    }

    doRequest(`${API_URL}v1/dashboard/${dashboardName}`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => {
            dispatch(updateDashboard(data));
        })
        .catch(err => {
            console.log(err);
        });
}