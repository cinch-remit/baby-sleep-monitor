import {API_URL, defaultHeaders, doRequest} from "../../httpClient";

export const SET_BUILD_INFO = 'SET_BUILD_INFO';
export const WELCOME_MESSAGE = 'SHOW_WELCOME';
export const GLOBAL_LOADING = 'GLOBAL_LOADING';

const setBuildInfo = payload => ({
    type: SET_BUILD_INFO,
    payload
});

const setShowWelcomeMessage = payload => ({
    type: WELCOME_MESSAGE,
    payload
})

const setGlobalLoading = payload => ({
    type: GLOBAL_LOADING,
    payload
});

export const doSetShowWelcomeMessage = payload => dispatch => {
    dispatch(setShowWelcomeMessage(payload))
}

export const doSetGlobalLoading = payload => dispatch => {
    dispatch(setGlobalLoading(payload));
}

export const doSetBuildInfo = () => dispatch => {
    const settings = {
        method: "GET",
        headers: defaultHeaders
    }

    doRequest(`${API_URL}app-info`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => dispatch(setBuildInfo(data)))
        .catch(err => console.log(err))
}