import {API_URL, defaultHeaders, doRequest} from "../../httpClient";

export const SET_DEVICES = 'SET_DEVICES';
export const UPDATE_DEVICE = 'UPDATE_DEVICE'
export const SELECT_DEVICE = 'SELECT_DEVICE';
export const CLEAR_DEVICE = 'CLEAR_DEVICE';

const setDevices = payload => ({
    type: SET_DEVICES,
    payload
});

const updateDevice = payload => ({
    type: UPDATE_DEVICE,
    payload
});

const clearDevice = () => ({
    type: CLEAR_DEVICE
})

export const doClearDevice = () => dispatch => {
    dispatch(clearDevice());
}

const selectDevice = payload => ({
    type: SELECT_DEVICE,
    payload
});

export const doSelectDevice = payload => dispatch => {
    dispatch(selectDevice(payload))
};

export const doSetDevices = devices => dispatch => {
    dispatch(setDevices(devices))
};

export const loadDevices = () => dispatch => {
    const authorization = localStorage.getItem('Authorization');
    if (!authorization) {
        return;
    }

    const settings = {
        method: 'GET',
        headers: {
            ...defaultHeaders,
            Authorization: authorization
        }
    }

    doRequest(`${API_URL}v1/device/all`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => {
            dispatch(setDevices(data));
            if (data.length) {
                dispatch(selectDevice(data[0].deviceID));
            }
        })
        .catch(err => {
            console.log(err);
        });
}

export const loadDevice = deviceId => dispatch => {
    const settings = {
        method: 'GET',
        headers: defaultHeaders
    }

    doRequest(`/v1/device/${deviceId}`, settings)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(data => dispatch(updateDevice(data)))
        .catch(err => {
            console.log(err);
        });
}