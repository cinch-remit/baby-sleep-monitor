export const validateEmail = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
};

export const validatePasswords = (password, passwordRepeat) => {
    return (
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/.test(password) &&
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/.test(passwordRepeat) && (password === passwordRepeat)
    );
};