import {LOAD_NOTIFICATIONS, LOGIN, CLEAR_USER, STATUS, UPDATE_NOTIFICATION} from "../../actions/user/userActions";

const defaultState = {
    user: {},
    status: false,
    notifications: [],
    loggedIn: false
}


const user = (state = defaultState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                user: action.payload,
                loggedIn: true
            }
        case CLEAR_USER:
            return defaultState;
        case STATUS:
            return {
                ...state,
                status: action.payload
            }
        case LOAD_NOTIFICATIONS: {
            return {
                ...state,
                notifications: action.payload
            }
        }
        case UPDATE_NOTIFICATION: {
            return {
                ...state,
                notifications: [action.payload, ...state.notifications]
            }
        }
        default:
            return state;
    }
}

export default user;