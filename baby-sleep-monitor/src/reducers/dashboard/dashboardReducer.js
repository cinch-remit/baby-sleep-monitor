import {
    CLEAR_DASHBOARDS,
    SELECT_DASHBOARD,
    SET_DASHBOARDS,
    UPDATE_DASHBOARD
} from "../../actions/dashboard/dashboardActions";

const defaultState = {
    dashboards: [],
    selectedDashboardName: ''
}

const dashboards = (state = defaultState, action) => {
    switch (action.type) {
        case SET_DASHBOARDS:
            let selectedDashboardName = state.selectedDashboardName;
            if (selectedDashboardName === '') {
                selectedDashboardName = action.payload[0].name;
            }
            return {
                ...state,
                dashboards: action.payload,
                selectedDashboardName: selectedDashboardName
            }

        case UPDATE_DASHBOARD:
            const data = action.payload;
            const dashboardsWithoutDashboardToBeUpdated = [...state.dashboards].filter(dashboard => dashboard.name !== data.name);
            return {
                ...state,
                dashboards: [...dashboardsWithoutDashboardToBeUpdated, data],
                selectedDashboardName: data.name
            }

        case CLEAR_DASHBOARDS:
            return defaultState;

        case SELECT_DASHBOARD:
            return {
                ...state,
                selectedDashboardName: action.payload
            }

        default:
            return state;
    }
}

export default dashboards;