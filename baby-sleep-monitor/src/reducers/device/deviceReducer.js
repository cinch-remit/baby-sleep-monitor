import {CLEAR_DEVICE, SELECT_DEVICE, SET_DEVICES, UPDATE_DEVICE} from "../../actions/device/deviceActions";

const defaultState = {
    devices: [],
    selectedDeviceId: ''
}

const devices = (state = defaultState, action) => {
    switch (action.type) {
        case SET_DEVICES:
            return {
                ...state,
                devices: action.payload
            }

        case UPDATE_DEVICE:
            const data = action.payload;
            const devicesWithoutDeviceToBeUpdated = [...state.devices].filter(device => device.deviceID !== data.deviceID)
            return {
                ...state,
                devices: [...devicesWithoutDeviceToBeUpdated, data]
            }

        case SELECT_DEVICE:
            return {
                ...state,
                selectedDeviceId: action.payload
            }

        case CLEAR_DEVICE:
            return defaultState;

        default:
            return state
    }
}

export default devices;